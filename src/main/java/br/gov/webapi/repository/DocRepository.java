package br.gov.webapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.webapi.model.Doc;

public interface DocRepository extends JpaRepository<Doc,Integer>{
	
}
