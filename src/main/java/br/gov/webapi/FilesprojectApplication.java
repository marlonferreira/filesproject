package br.gov.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilesprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilesprojectApplication.class, args);
	}

}
